quantitate intra-tumor heterogeneity from a single tumor bulk sequencing:

>Clinical data and NGS exome-sequencing results (approximately 1% of the genome, at 150-fold mean sequence coverage) for 74 HNSCC, and data on genomic copy-number alterations (CNA) for 55 of these, were imported from Supplementary Tables 6, 10 and 11 of Stransky et al.10 into R.11 Each tumor’s MATH value was calculated from the median absolute deviation (MAD) and the median of its mutant-allele fractions at tumor-specific mutated loci:


`MATH=100*MAD/median`

> Calculation of MAD followed the default in R, with values scaled by a constant factor (1.4826) so that the expected MAD of a sample from a normal distribution equals the standard deviation.

reference: [MATH, a novel measure of intratumor genetic heterogeneity, is high in poor-outcome classes of head and neck squamous cell carcinoma](http://www.sciencedirect.com/science/article/pii/S1368837512002977)